FROM alpine

RUN apk update && apk add  curl jq  poppler-utils git 
RUN mkdir cache

WORKDIR /app



COPY . /app

CMD ["/bin/bash", "/app/start.sh"]
