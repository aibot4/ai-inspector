#!/bin/bash
while true
do
echo "started main loop"
echo $?
sleep 0.5
git pull
# Define the command to run (replace with your command)
COMMAND="bash tele.sh"
timeout 1m $COMMAND
# Check the exit code of the command
if [ $? -eq 124 ]; then
    echo "Command timed out"
# Define the Telegram Bot API token
TOKEN=$(cat token)
# Define the chat ID
CHAT_ID=$(cat id)
# Send a message to the chat
curl -s -X POST "https://api.telegram.org/bot$TOKEN/sendMessage" \
     -d chat_id="$CHAT_ID" \
     -d text="The command has timed out. Please restart."
else
    echo "Command completed successfully"
fi
done
